package mmiit.wahda.view.models;

import android.databinding.BaseObservable;
import android.databinding.DataBindingComponent;
import android.databinding.ViewDataBinding;

import org.parceler.Transient;

import mmiit.wahda.services.rest.AbstractCallback;
import mmiit.wahda.services.rest.request.UpdloadLogRequest;
import mmiit.wahda.services.rest.response.BaseResponse;
import mmiit.wahda.view.models.components.AvatarBindingComponent;
import mmiit.wahda.view.models.components.GalleryBindingComponent;
import mmiit.wahda.view.models.components.ImageBindingComponent;
import mmiit.wahda.view.models.components.GridBindingComponent;
import mmiit.wahda.view.models.components.ListBindingComponent;


/**
 * Created by MMIIT on 8/4/2018.
 */
public abstract class AbstractViewModel extends BaseObservable implements DataBindingComponent {

    @Transient
    private ViewDataBinding mBinder;

    public void setBinder(ViewDataBinding binder) {
        this.mBinder = mBinder;
    }

    protected ViewDataBinding getBinder() {
        return mBinder;
    }

    public abstract String getModelId();

    public abstract int getLayoutId();

    @Override
    public ListBindingComponent getListBindingComponent() {
        return null;
    }

    @Override
    public AvatarBindingComponent getAvatarBindingComponent() {
        return null;
    }

    @Override
    public ImageBindingComponent getImageBindingComponent() {
        return null;
    }

    @Override
    public GridBindingComponent getGridBindingComponent() {
        return null;
    }

    @Override
    public GalleryBindingComponent getGalleryBindingComponent() {
        return null;
    }
}
