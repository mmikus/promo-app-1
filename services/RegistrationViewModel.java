package mmiit.wahda.view.models;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DatabaseErrorHandler;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mmiit.wahda.activities.chat.ChatRoomsActivity;
import mmiit.wahda.R;
import mmiit.wahda.data.Database;
import mmiit.wahda.services.ApplicationLifecycleService;
import mmiit.wahda.services.rest.AbstractCallback;
import mmiit.wahda.services.rest.json.Contact;
import mmiit.wahda.services.rest.request.AddContactRequest;
import mmiit.wahda.services.rest.request.AddNewContactsRequest;
import mmiit.wahda.services.rest.request.FindContactRequest;
import mmiit.wahda.services.rest.request.FindContactsRequest;
import mmiit.wahda.services.rest.request.ListMyContactsRequest;
import mmiit.wahda.services.rest.request.RegisterRequest;
import mmiit.wahda.services.rest.response.BaseResponse;
import mmiit.wahda.services.rest.response.ContactListResponse;
import mmiit.wahda.services.rest.response.ContactResponse;
import mmiit.wahda.services.xmpp.XmppService;
import mmiit.wahda.services.xmpp.message.AbstractXmppMessage;
import mmiit.wahda.services.xmpp.message.SendMessage;
import mmiit.wahda.services.xmpp.message.SubscribeContactMessage;
import mmiit.wahda.utils.Animations;
import mmiit.wahda.utils.Broadcaster;
import mmiit.wahda.utils.DeviceInfo;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.PrettyDate;
import mmiit.wahda.utils.Validations;
import mmiit.wahda.utils.WahdaEncryptor;
import mmiit.wahda.view.adapters.ContactsAdapter;
import mmiit.wahda.view.adapters.CountryPhoneAdapter;
import mmiit.wahda.view.models.chat.ChatViewItemModelView;
import mmiit.wahda.view.models.chat.ContactItemModelView;
import mmiit.wahda.view.models.components.ListBindingComponent;

/**
 * Created by MMIIT on 8/4/2018.
 */
@Parcel
public class RegistrationViewModel extends AbstractViewModel implements ListBindingComponent {

    private String country;
    private String code;
    private String prefix;
    private String phoneNumber;
    private int layoutId;
    private ObservableArrayList<CountryPhoneItemModelView> countries;
    private ObservableArrayList<ContactItemModelView> contactList;

    public RegistrationViewModel() {
    }

    public RegistrationViewModel(int layoutId) {
        this.layoutId = layoutId;
    }

    @Bindable
    public boolean isCountrySelected() {
        return country != null;
    }

    @Bindable
    public String getCountry() {
        return country;
    }

    @Bindable
    public String getPrefix() {
        return prefix;
    }

    @Bindable
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void fetchCurrentCountryCode(String code) {
        if (this.prefix == null) {
            this.prefix = Database.registration().getPrefix(code.toUpperCase());
            Locale country = new Locale("", code);
            this.code = code.toUpperCase();
            this.country = country.getDisplayCountry();
        }
    }

    @Override
    public void bindList(ListView view, Object list, Context context) {
        if (((ObservableArrayList) list).size() > 0) {
            if (((ObservableArrayList) list).get(0) instanceof CountryPhoneItemModelView) {
                final ListAdapter adapter = new CountryPhoneAdapter((ObservableArrayList<CountryPhoneItemModelView>) list);
                view.setAdapter(adapter);
                view.setOnItemClickListener((adapterView, view1, i, l) -> {
                    this.setCountry((CountryPhoneItemModelView) adapter.getItem(i));
                    Animations.backToActivity(this);
                });
            } else if (((ObservableArrayList) list).get(0) instanceof ContactItemModelView) {
                final ListAdapter adapter = new ContactsAdapter((ObservableArrayList<ContactItemModelView>) list);
                view.setAdapter(adapter);
                view.setOnItemClickListener((adapterView, view1, i, l) -> {
                    ContactItemModelView item = (ContactItemModelView) adapter.getItem(i);
                    if (!Database.contacts().hasContact(item.getPhoneNumber())) {
                        AddContactRequest request = new AddContactRequest(new ArrayList<String>() {{
                            add(WahdaEncryptor.encryptPhoneNumber(item.getPhoneNumber()));
                        }}, context);
                        request.send(new AbstractCallback<ContactListResponse>(request, context) {
                            @Override
                            public void onSuccess(ContactListResponse response) {
                                if (response.isSuccess() && response.getData().size() > 0) {
                                    List<Contact> data = response.getData();
                                    Database.contacts().addContacts(data, context.getContentResolver());
                                    List<String> jids = Lists.transform(data, input -> input.getJid()/*DeviceInfo.toJid(input.getPhoneNumber())*/);
                                    Broadcaster.xmpp(Parcels.wrap(new SubscribeContactMessage(jids)));
                                    Animations.backToActivity();
                                } else if (response.getData().size() == 0) {
                                    Alerts.error(R.string.toast_user_not_found_error);
                                } else {
                                    Alerts.error(R.string.toast_server_error);
                                }
                            }
                        });
                    } else {
                        Alerts.error(R.string.toast_contact_already_added);
                    }
                });
            }
        }
    }

    public void filter(String country) {
        List<CountryPhoneItemModelView> buff = Database.registration().getCountriesPhoneNumbers();
        CollectionUtils.filter(buff, model -> model.getCountry().toLowerCase().contains(country.toLowerCase()));
        countries = new ObservableArrayList<CountryPhoneItemModelView>() {{
            addAll(buff);
        }};
        notifyPropertyChanged(BR._all);
    }

    public ObservableArrayList<CountryPhoneItemModelView> listCountryPhoneCodes() {
        if (countries == null) {
            countries = new ObservableArrayList<CountryPhoneItemModelView>() {{
                addAll(Database.registration().getCountriesPhoneNumbers());
            }};
        }
        return countries;
    }

    public ObservableArrayList<ContactItemModelView> listFoundContacts() {
        if (contactList == null) {
            contactList = new ObservableArrayList<>();
        }
        return contactList;
    }

    public void setCountry(CountryPhoneItemModelView country) {
        this.country = country.getCountry();
        this.code = country.getCode();
        this.prefix = country.getPrefix();
        notifyPropertyChanged(BR.country);
        notifyPropertyChanged(BR.prefix);
    }

    public void addContact(Context context) {
        if (prefix == null || phoneNumber == null) {
            Alerts.error(R.string.toast_null_member_phone_error);
        } else {
            String number = new StringBuilder(prefix).append(phoneNumber).toString();
            if (Validations.isPhoneNumber(number, code)) {
                if (number.equals(Database.registration().getAccount().getPhoneNumber())) {
                    Alerts.error(R.string.toast_self_user_search_not_allowed);
                } else {
                    contactList.clear();
                    FindContactsRequest request = new FindContactsRequest(WahdaEncryptor.encryptPhoneNumber(Validations.toInternationalFormat(number, code)), context);
                    request.send(new AbstractCallback<ContactListResponse>(request, context) {
                        @Override
                        public void onSuccess(ContactListResponse response) {
                            if (response.isSuccess()) {
                                List<Contact> contacts = response.getData();
                                for(Contact data : contacts) {
                                    String name = null;
                                    if (data.getUsername() != null && !data.getUsername().isEmpty()) {
                                        name = data.getUsername();
                                    } else {
                                        name = Database.contacts().getLocalName(context.getContentResolver(), data.getPhoneNumber());
                                        if (name == null) {
                                            name = data.getPhoneNumber();
                                        }
                                    }

                                    contactList.add(new ContactItemModelView(
                                            data.getId(),
                                            data.isHidden(),
                                            data.getJid(),
                                            name,
                                            PrettyDate.toPretty(data.getLastLogin()),
                                            data.getAvatarUrl(),
                                            data.getPhoneNumber(),
                                            data.isBan(),
                                            false
                                    ));
                                }
                            } else {
                                Alerts.error(R.string.toast_user_not_found_error);
                            }
                        }
                    });
                }
            } else {
                Alerts.error(R.string.toast_phone_number_wrong_format_error);
            }
        }
    }

    public void save(Context context) {
        if (prefix == null || phoneNumber == null) {
            Alerts.error(R.string.toast_null_phone_error);
        } else if (DeviceInfo.getImei(context) == null) {
            Alerts.error(R.string.toast_imei_not_readed);
        } else {
            String number = new StringBuilder(prefix).append(phoneNumber).toString();
            if (Validations.isPhoneNumber(number, code)) {
                RegisterRequest request = new RegisterRequest(Validations.toInternationalFormat(number, code), context);
                request.send(new AbstractCallback<BaseResponse>(request, context) {
                    @Override
                    public void onSuccess(BaseResponse response) {
                        if (response.isSuccess() || response.getStatusCode() == 403) {
                            Database.registration().register(request.getAuthorization());
                            Intent xmppService = new Intent(context, XmppService.class);
                            context.startService(xmppService);
//                            Intent gcmRegistration = new Intent(context, RegistrationIntentService.class);
//                            context.startService(gcmRegistration);
                            List<String> locals = Database.contacts().listLocalPhoneNumbers(context);
                            AddNewContactsRequest request = new AddNewContactsRequest(locals, context);
                            ApplicationLifecycleService.setRegistered(true);
                            request.setRunBackground(true);
                            request.send(new AbstractCallback<ContactListResponse>(request, context) {
                                @Override
                                public void onSuccess(ContactListResponse response) {
                                    if (response.isSuccess()) {
                                        Database.contacts().addContacts(response.getData(), context.getContentResolver());
                                    }
                                }
                            });
                            if (response.getStatusCode() == 403) {
                                new Thread(() -> {
                                    ListMyContactsRequest myContactsRequest = new ListMyContactsRequest(context);
                                    request.setRunBackground(true);
                                    request.send(new AbstractCallback<ContactListResponse>(myContactsRequest, context) {
                                        @Override
                                        public void onSuccess(ContactListResponse response) {
                                            if (response.isSuccess()) {
                                                Database.contacts().syncContact(response.getData(), context.getContentResolver());
                                            }
                                        }
                                    });
                                }).run();
                            }
                            Animations.toActivity(ChatRoomsActivity.class);
                        } else {
                            Alerts.error(R.string.toast_registration_error);
                        }
                    }
                });
            } else {
                Alerts.error(R.string.toast_phone_number_wrong_format_error);
            }
        }

    }

    @Override
    public String getModelId() {
        return RegistrationViewModel.class.getName();
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public ListBindingComponent getListBindingComponent() {
        return this;
    }
}
