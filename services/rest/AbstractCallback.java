package mmiit.wahda.services.rest;


import android.content.Context;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

import mmiit.wahda.data.Database;
import mmiit.wahda.services.rest.request.AuthorizeRequest;
import mmiit.wahda.services.rest.request.BaseRequest;
import mmiit.wahda.services.rest.response.AbstractResponse;
import mmiit.wahda.utils.CircleBar;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by MMIIT on 9/2/2018.
 */
public abstract class AbstractCallback<T extends AbstractResponse> implements Callback {
    private BaseRequest mRequest;
    private Context mContext;
    private Class<T> mResponse;

    public AbstractCallback(BaseRequest request, Context context) {
        this.mRequest = request;
        this.mContext = context;
        this.mResponse = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void onFailure(Call call, IOException e) {
        try {
            CircleBar.hide();
            onSuccess(mResponse.newInstance());
        } catch (InstantiationException | IllegalAccessException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onResponse(Call call, Response response) {
        if (response.isSuccessful()) {
            try {
                CircleBar.hide();
                this.onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        } else if (response.code() == 401) {
            AuthorizeRequest request = new AuthorizeRequest(Database.registration().getAccount().getPhoneNumber(), mContext);
            request.setRunBackground(true);
            request.send(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    try {
                        CircleBar.hide();
                        onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
                    } catch (InstantiationException | IllegalAccessException e1) {
                        e1.printStackTrace();
                    } catch (NoSuchMethodException e1) {
                        e1.printStackTrace();
                    } catch (InvocationTargetException e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        mRequest.setRunBackground(true);
                        mRequest.send(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                try {
                                    CircleBar.hide();
                                    onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
                                } catch (InstantiationException | IllegalAccessException e1) {
                                    e1.printStackTrace();
                                } catch (NoSuchMethodException e1) {
                                    e1.printStackTrace();
                                } catch (InvocationTargetException e1) {
                                    e1.printStackTrace();
                                }
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    try {
                                        CircleBar.hide();
                                        onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
                                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        CircleBar.hide();
                                        onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
                                    } catch (InstantiationException | IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (NoSuchMethodException e) {
                                        e.printStackTrace();
                                    } catch (InvocationTargetException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    } else {
                        try {
                            CircleBar.hide();
                            onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
                        } catch (InstantiationException | IllegalAccessException e1) {
                            e1.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else {
            try {
                CircleBar.hide();
                onSuccess(mResponse.getDeclaredConstructor(Response.class).newInstance(response));
            } catch (InstantiationException | IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public abstract void onSuccess(T response);
}
