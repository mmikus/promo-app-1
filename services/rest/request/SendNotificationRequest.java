package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import mmiit.wahda.R;
import mmiit.wahda.services.rest.json.Notification;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.DeviceInfo;
import mmiit.wahda.utils.WahdaEncryptor;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 10/6/2018.
 */
public class SendNotificationRequest extends BaseRequest {
    private static final String METHOD = "sendNotification";
    private Notification mNotification;
    private String mTo;

    public SendNotificationRequest(Context context, Notification notification, String to) {
        super(context);
        this.mNotification = notification;
        this.mTo = to;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Gson gson = new Gson();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), gson.toJson(mNotification));
            Request request = new Request.Builder().url(buildUrl(USER, METHOD, new String[]{"to", WahdaEncryptor.encryptPhoneNumber(mTo)})).post(requestBody).build();
            getClient().newCall(request).enqueue(callback);
        }
    }
}
