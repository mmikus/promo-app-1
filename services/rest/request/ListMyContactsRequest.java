package mmiit.wahda.services.rest.request;

import android.content.Context;

import mmiit.wahda.R;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.Request;

/**
 * Created by MMIIT on 9/23/2018.
 */
public class ListMyContactsRequest extends BaseRequest {
    public static final String METHOD = "listMyContacts";

    public ListMyContactsRequest(Context context) {
        super(context);
    }//أمينة

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).get().build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
