package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mmiit.wahda.R;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 9/23/2018.
 */
public class AddContactRequest extends BaseRequest {
    public static final String METHOD = "addContacts";
    private List<String> mUserIds;

    public AddContactRequest(List<String> mUserIds, Context context) {
        super(context);
        this.mUserIds = mUserIds;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Gson gson = new Gson();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), gson.toJson(mUserIds.toArray()));
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).post(requestBody).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
