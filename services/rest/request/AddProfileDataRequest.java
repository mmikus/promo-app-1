package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import mmiit.wahda.R;
import mmiit.wahda.data.Database;
import mmiit.wahda.services.rest.json.Log;
import mmiit.wahda.services.rest.json.Profile;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 10/16/2018.
 */
public class AddProfileDataRequest extends BaseRequest {

    private static final String METHOD = "addProfileData";
    private Profile mProfile;

    public AddProfileDataRequest(Context context, Profile profile) {
        super(context);
        this.mProfile = profile;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Gson gson = new Gson();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), gson.toJson(mProfile));
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).post(requestBody).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
