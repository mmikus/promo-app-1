package mmiit.wahda.services.rest.request;

import android.content.Context;

import mmiit.wahda.R;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.Request;

/**
 * Created by MMIIT on 2/26/2019.
 */
public class FindContactsRequest extends BaseRequest {
    public static final String METHOD = "findContacts";
    private String mPhoneNumberHash;

    public FindContactsRequest(String phoneNumberHash, Context context) {
        super(context);
        this.mPhoneNumberHash = phoneNumberHash;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Request request = new Request.Builder()
                    .url(buildUrl(USER, METHOD, new String[]{"phoneNumberHash", mPhoneNumberHash})).get().build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
