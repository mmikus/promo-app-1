package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;

import mmiit.wahda.R;
import mmiit.wahda.services.rest.json.Contact;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 9/23/2018.
 */
public class ChangeOnlineStateRequest extends BaseRequest {
    public static final String METHOD = "changeMyOnlineState";
    private List<Contact> mContacts;

    public ChangeOnlineStateRequest(List<Contact> mContacts, Context context) {
        super(context);
        this.mContacts = mContacts;
    }


    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Gson gson = new Gson();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), gson.toJson(mContacts.toArray()));
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).post(requestBody).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
