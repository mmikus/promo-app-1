package mmiit.wahda.services.rest.request;

import android.content.Context;

import mmiit.wahda.R;
import mmiit.wahda.services.rest.response.AbstractResponse;
import mmiit.wahda.services.rest.response.GetSessionsResponse;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by MMIIT on 9/2/2018.
 */
public class GetSessionsRequest extends BaseRequest<GetSessionsResponse> {
    private static final String METHOD = "mySessions";

    public GetSessionsRequest(Context context) {
        super(context);
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).get().build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
