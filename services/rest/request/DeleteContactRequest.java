package mmiit.wahda.services.rest.request;

import android.content.Context;

import mmiit.wahda.R;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import mmiit.wahda.utils.WahdaEncryptor;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 10/13/2018.
 */
public class DeleteContactRequest extends BaseRequest {
    private static final String METHOD = "deleteContact";
    private String mPhoneNumber;

    public DeleteContactRequest(Context context, String phoneNumber) {
        super(context);
        this.mPhoneNumber = phoneNumber;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Request request = new Request.Builder().url(buildUrl(USER, METHOD,
                    new String[]{"number", WahdaEncryptor.encryptPhoneNumber(mPhoneNumber)}))
                    .post(RequestBody.create(null, "")).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
