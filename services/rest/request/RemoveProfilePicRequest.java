package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import mmiit.wahda.R;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 10/16/2018.
 */
public class RemoveProfilePicRequest extends BaseRequest {
    public static final String METHOD = "removeProfilePic";

    public RemoveProfilePicRequest(Context context) {
        super(context);
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            RequestBody requestBody = RequestBody.create(null, "");
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).post(requestBody).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
