package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;

import mmiit.wahda.R;
import mmiit.wahda.data.Database;
import mmiit.wahda.services.rest.json.Log;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 9/28/2018.
 */
public class UpdloadLogRequest extends BaseRequest {
    private static final String METHOD = "uploadLogs";

    public UpdloadLogRequest(Context context) {
        super(context);
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            List<Log> logs = Database.logs().listLogs();
            Gson gson = new Gson();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), gson.toJson(logs));
            Request request = new Request.Builder().url(buildUrl(USER, METHOD)).post(requestBody).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
