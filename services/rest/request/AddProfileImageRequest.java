package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.common.io.ByteStreams;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import mmiit.wahda.R;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 10/16/2018.
 */
public class AddProfileImageRequest extends BaseRequest {
    private static final String METHOD = "addProfilePic";

    private InputStream mFile;

    public AddProfileImageRequest(Context context, InputStream file) {
        super(context);
        this.mFile = file;
    }

    @Override
    public void send(Callback callback) {
        try {
            if (!DeviceInfo.hasInternetConnection(mContext)) {
                Alerts.error(R.string.toast_no_internet_connection);
            } else {
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), ByteStreams.toByteArray(mFile));
                Request request = new Request.Builder().url(buildUrl(USER, METHOD)).put(requestBody).build();
                if (!mRunBackground) {
                    CircleBar.show(mContext);
                }
                getClient().newCall(request).enqueue(callback);
            }
        } catch (IOException e) {
            e.printStackTrace();
            CircleBar.hide();
        }

    }
}
