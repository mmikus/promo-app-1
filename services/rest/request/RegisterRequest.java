package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import mmiit.wahda.R;
import mmiit.wahda.services.rest.json.Authorization;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 8/15/2018.
 */
public class RegisterRequest extends BaseRequest {
    private static final String METHOD = "register";
    private Authorization authorization;

    public RegisterRequest(String phoneNumber, Context context) {
        super(context);
        this.authorization = new Authorization(phoneNumber, mContext);
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Gson gson = new Gson();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), gson.toJson(authorization));
            Request request = new Request.Builder().url(buildUrl(AUTH, METHOD)).post(requestBody).build();
            if (!mRunBackground) {
                CircleBar.show(mContext);
            }
            getClient().newCall(request).enqueue(callback);
        }
    }
}
