package mmiit.wahda.services.rest.request;

import android.content.Context;

import java.net.CookieManager;
import java.net.CookiePolicy;

import mmiit.wahda.services.rest.response.AbstractResponse;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;

/**
 * Created by MMIIT on 8/15/2018.
 */
public abstract class BaseRequest<T extends AbstractResponse> {
    public static final String SCHEME = "http";
    public static final String HOST = "185.33.144.229";//"194.182.84.65";
    public static final int PORT = 2376;//8080;
    public static final String AUTHORITY = "185.33.144.229:2376";//"194.182.84.65:8080";
    private static final String BASE_URL = "http://185.33.144.229:2376/wahda-web/rest";//"http://194.182.84.65:8080/wahda-web/rest";
    protected static final String AUTH = "auth";
    protected static final String USER = "user";
    private static OkHttpClient mClient;
    protected Context mContext;
    protected boolean mRunBackground = false;


    public abstract void send(Callback callback);

    public BaseRequest(Context context) {
        this.mContext = context;
    }

    public void setRunBackground(boolean runBackground) {
        this.mRunBackground = runBackground;
    }

    protected HttpUrl buildUrl(String endpoint, String method, String[]... parameters) {
        HttpUrl.Builder builder = new HttpUrl.Builder().scheme(SCHEME)
                .host(HOST)
                .port(PORT)
                .addPathSegment("wahda-web")
                .addPathSegment("rest")
                .addPathSegment(endpoint)
                .addPathSegment(method);

        for (String[] parameter : parameters) {
            builder.addQueryParameter(parameter[0], parameter[1]);
        }
        return builder.build();
    }

    protected HttpUrl buildUrl(String endpoint, String method) {
        return new HttpUrl.Builder().scheme(SCHEME)
                .host(HOST)
                .port(PORT)
                .addPathSegment("wahda-web")
                .addPathSegment("rest")
                .addPathSegment(endpoint)
                .addPathSegment(method).build();
    }

    protected OkHttpClient getClient() {
        if (mClient == null) {
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy((uri, cookie) -> {
                if (!AUTHORITY.equals(uri.getAuthority())) {
                    return false;
                }
                return CookiePolicy.ACCEPT_ORIGINAL_SERVER.shouldAccept(uri, cookie);
            });
            mClient = new OkHttpClient.Builder().cookieJar(new JavaNetCookieJar(cookieManager)).build();
        }

        return mClient;
    }
}
