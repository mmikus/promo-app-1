package mmiit.wahda.services.rest.request;

import android.content.Context;

import com.google.gson.Gson;

import mmiit.wahda.R;
import mmiit.wahda.services.rest.json.Notification;
import mmiit.wahda.utils.Alerts;
import mmiit.wahda.utils.CircleBar;
import mmiit.wahda.utils.DeviceInfo;
import mmiit.wahda.utils.WahdaEncryptor;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by MMIIT on 10/6/2018.
 */
public class RegisterGCMTokenRequest extends BaseRequest {
    private static final String METHOD = "registerNotificationToken";
    private String mToken;

    public RegisterGCMTokenRequest(Context context, String token) {
        super(context);
        this.mToken = token;
    }

    @Override
    public void send(Callback callback) {
        if (!DeviceInfo.hasInternetConnection(mContext)) {
            Alerts.error(R.string.toast_no_internet_connection);
        } else {
            Request request = new Request.Builder().url(buildUrl(AUTH, METHOD, new String[]{"token", mToken})).post(RequestBody.create(null, "")).build();
            getClient().newCall(request).enqueue(callback);
        }
    }
}
