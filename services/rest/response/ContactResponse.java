package mmiit.wahda.services.rest.response;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import mmiit.wahda.services.rest.json.Contact;
import okhttp3.Response;

/**
 * Created by MMIIT on 9/23/2018.
 */
public class ContactResponse extends AbstractResponse {
    public ContactResponse() {
    }

    public ContactResponse(Response response) {
        super(response);
    }

    @Override
    public Contact getData() {
        Type type = new TypeToken<Contact>() {
        }.getType();
        return mGson.fromJson(getJson().getAsJsonObject("data"), type);
    }
}
