package mmiit.wahda.services.rest.response;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mmiit.wahda.services.rest.json.Session;
import okhttp3.Response;

/**
 * Created by MMIIT on 9/4/2018.
 */
public class GetSessionsResponse extends AbstractResponse {

    public GetSessionsResponse() {
    }

    public GetSessionsResponse(Response response) {
        super(response);
    }

    @Override
    public List<Session> getData() {
        Type type = new TypeToken<ArrayList<Session>>() {
        }.getType();
        return mGson.fromJson(getJson().getAsJsonArray("data"), type);
    }
}
