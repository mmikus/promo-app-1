package mmiit.wahda.services.rest.response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by MMIIT on 9/3/2018.
 */
public abstract class AbstractResponse {

    private int mStatusCode;
    private String mJson;
    protected Response mResponse;
    protected Gson mGson;

    public AbstractResponse() {
        this.mGson = new Gson();
    }

    public AbstractResponse(Response response) {
        this.mStatusCode = response.code();
        this.mResponse = response;
        this.mGson = new Gson();
        try {
            this.mJson = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public boolean isSuccess() {
        return mStatusCode == 200;
    }

    protected JsonObject getJson() {
        JsonParser parser = new JsonParser();
        return parser.parse(mJson).getAsJsonObject();
    }

    public Object getData() throws IOException {
        return null;
    }
}
