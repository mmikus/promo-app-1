package mmiit.wahda.services.rest.response;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mmiit.wahda.services.rest.json.Contact;
import okhttp3.Response;

/**
 * Created by MMIIT on 9/23/2018.
 */
public class ContactListResponse extends AbstractResponse {
    public ContactListResponse() {
    }

    public ContactListResponse(Response response) {
        super(response);
    }

    @Override
    public List<Contact> getData() {
        Type type = new TypeToken<ArrayList<Contact>>() {
        }.getType();
        return mGson.fromJson(getJson().getAsJsonArray("data"), type);
    }
}
