package mmiit.wahda.services.rest.response;

import okhttp3.Response;

/**
 * Created by MMIIT on 9/23/2018.
 */
public class BaseResponse extends AbstractResponse {
    public BaseResponse() {
    }

    public BaseResponse(Response response) {
        super(response);
    }
}
