package mmiit.wahda.services.rest.json;

import java.io.Serializable;

/**
 * Created by MMIIT on 10/16/2018.
 */
public class Profile implements Serializable {
    private String username;

    public Profile() {
    }

    public Profile(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
