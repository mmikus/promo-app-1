package mmiit.wahda.services.rest.json;

import java.io.Serializable;

import mmiit.wahda.enums.FileTypeEnum;

/**
 * Created by MMIIT on 10/8/2018.
 */
public class File implements Serializable {

    private String name;
    private FileTypeEnum type;
    private Long length;
    private String path;

    public File(String name, Long length, String path, FileTypeEnum type) {
        this.name = name;
        this.length = length;
        this.path = path;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FileTypeEnum getType() {
        return type;
    }

    public void setType(FileTypeEnum type) {
        this.type = type;
    }
}
