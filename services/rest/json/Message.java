package mmiit.wahda.services.rest.json;

import android.net.Uri;

import com.google.gson.Gson;

import org.webrtc.SessionDescription;									 
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by MMIIT on 9/25/2018.
 */
public class Message implements Serializable {
    private String roomId;
    private String message;
    private String stanzaId;
    private String password;
    private boolean secured;
    private boolean isRoomSecured;
    private String roomPassword;
    private Contact from;
    private boolean isRoomDeleted;
    private boolean isMessageDeleted;
    private boolean isSelfDestruct;
    private boolean isPostpone;
    private long milliseconds;
    private List<Contact> recipients;
    private boolean isEdited;
    private File file;
    private boolean isInvitation;
    private String groupName;
    private boolean isUserState;
    private boolean isUserHidden;
	private String videoMessage;
    private String videoMessageType; 																	   

    public Message() {
    }

    public Message(boolean isUserState, Contact from, Contact to) {
        this.isUserState = isUserState;
        this.from = from;
        this.recipients = new ArrayList<>();
        this.recipients.add(to);
        this.stanzaId = UUID.randomUUID().toString();
    }

    public Message(boolean isUserState, Contact from) {
        this.isUserState = isUserState;
        this.from = from;
    }

    public Message(String roomId, Contact from, List<Contact> recipients) {
        this.roomId = roomId;
        this.from = from;
        this.recipients = recipients;
    }

    public Message(String roomId, boolean isRoomDeleted, List<Contact> recipients) {
        this.roomId = roomId;
        this.isRoomDeleted = isRoomDeleted;
        this.recipients = recipients;
        this.stanzaId = UUID.randomUUID().toString();
    }

    public Message(boolean isMessageDeleted, String stanzaId, List<Contact> recipients) {
        this.stanzaId = stanzaId;
        this.isMessageDeleted = isMessageDeleted;
        this.recipients = recipients;
    }

    public Message(String roomId, String message, String stanzaId, Contact from, List<Contact> recipients) {
        this.roomId = roomId;
        this.stanzaId = stanzaId;
        this.message = message;
        this.from = from;
        this.recipients = recipients;
    }

    public Message(String roomId, String message, String stanzaId,
                   String password, boolean secured, Contact from, List<Contact> recipients) {
        this.roomId = roomId;
        this.message = message;
        this.stanzaId = stanzaId;
        this.password = password;
        this.secured = secured;
        this.from = from;
        this.recipients = recipients;
    }

    public Message(String roomId, String message, String stanzaId, String password,
                   boolean secured, boolean isRoomSecured, String roomPassword,
                   Contact from, List<Contact> recipients, String roomName) {
        this.roomId = roomId;
        this.message = message;
        this.stanzaId = stanzaId;
        this.password = password;
        this.secured = secured;
        this.isRoomSecured = isRoomSecured;
        this.roomPassword = roomPassword;
        this.from = from;
        this.recipients = recipients;
        this.groupName = roomName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Contact getFrom() {
        return from;
    }

    public void setFrom(Contact contact) {
        this.from = contact;
    }

    public String getStanzaId() {
        return stanzaId;
    }

    public void setStanzaId(String stanzaId) {
        this.stanzaId = stanzaId;
    }

    public List<Contact> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<Contact> recipients) {
        this.recipients = recipients;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public boolean isRoomSecured() {
        return isRoomSecured;
    }

    public void setRoomSecured(boolean roomSecured) {
        isRoomSecured = roomSecured;
    }

    public String getRoomPassword() {
        return roomPassword;
    }

    public void setRoomPassword(String roomPassword) {
        this.roomPassword = roomPassword;
    }

    public boolean isRoomDeleted() {
        return isRoomDeleted;
    }

    public void setRoomDeleted(boolean roomDeleted) {
        isRoomDeleted = roomDeleted;
    }

    public boolean isMessageDeleted() {
        return isMessageDeleted;
    }

    public void setMessageDeleted(boolean messageDeleted) {
        isMessageDeleted = messageDeleted;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    public boolean isSelfDestruct() {
        return isSelfDestruct;
    }

    public void setSelfDestruct(boolean selfDestruct) {
        isSelfDestruct = selfDestruct;
    }

    public boolean isPostpone() {
        return isPostpone;
    }

    public void setPostpone(boolean postpone) {
        isPostpone = postpone;
    }

    public long getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(Long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isInvitation() {
        return isInvitation;
    }

    public void setInvitation(boolean invitation) {
        isInvitation = invitation;
    }

    public String getGroupName() {
        return groupName;
    }

	    public String getVideoMessage() {
        return this.videoMessage;
    }

    public void setVideoMessage(String videoMessage) {
        this.videoMessage = videoMessage;
    }

    public String getVideoMessageType() {
        return this.videoMessageType;
    }

    public void setVideoMessageType(String type) {
        this.videoMessageType = type;
    }							 
	
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

	public boolean isVideo(){
        return videoMessage != null && !videoMessage.isEmpty();
    }
    public boolean isUserState() {
        return isUserState;
    }

    public void setUserState(boolean userState) {
        isUserState = userState;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static Message fromJson(String message) {
        Gson gson = new Gson();
        return gson.fromJson(message, Message.class);
    }

    public boolean hasFile() {
        return file != null;
    }

    public boolean isUserHidden() {
        return isUserHidden;
    }

    public void setUserHidden(boolean userHidden) {
        isUserHidden = userHidden;
    }

}
