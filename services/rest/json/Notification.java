package mmiit.wahda.services.rest.json;

import java.io.Serializable;

/**
 * Created by MMIIT on 10/6/2018.
 */
public class Notification implements Serializable {
    private String roomJid;
    private String from;
    private String message;
    private Message data;

    public Notification() {
    }

    public Notification(String roomJid, String from, String message, Message data) {
        this.roomJid = roomJid;
        this.from = from;
        this.message = message;
        this.data = data;
    }

    public String getRoomJid() {
        return roomJid;
    }

    public void setRoomJid(String roomJid) {
        this.roomJid = roomJid;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message getData() {
        return data;
    }

    public void setData(Message data) {
        this.data = data;
    }
}
