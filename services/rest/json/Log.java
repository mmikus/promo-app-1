package mmiit.wahda.services.rest.json;

import java.io.Serializable;

/**
 * Created by MMIIT on 9/28/2018.
 */
public class Log implements Serializable {
    private Long remoteId;
    private String phoneNumber;
    private String error;
    private Long timestamp;

    public Log() {
    }

    public Log(Long remoteId, String phoneNumber, String error, Long timestamp) {
        this.remoteId = remoteId;
        this.phoneNumber = phoneNumber;
        this.error = error;
        this.timestamp = timestamp;
    }

    public Long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
