package mmiit.wahda.services.rest.json;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import mmiit.wahda.utils.DeviceInfo;
import mmiit.wahda.utils.WahdaEncryptor;

/**
 * Created by MMIIT on 8/15/2018.
 */
public class Authorization implements Serializable {
    private String os;
    private String device;
    private String version;
    private String ipAddress;
    private String location;
    private String phoneNumberHash;
    private String imeiHash;
    private String userName;
    private boolean isChanged;

    public Authorization() {
    }

    public Authorization(String phoneNumber, Context context, boolean isChanged) {
        this.phoneNumberHash = WahdaEncryptor.encryptPhoneNumber(phoneNumber);
        this.os = "Android";
        this.device = Build.MODEL;
        this.version = String.valueOf(Build.VERSION.SDK_INT);
        this.ipAddress = DeviceInfo.getIpAddress();
        this.location = DeviceInfo.getLocation(context);
        this.imeiHash = WahdaEncryptor.encryptImeiCode(DeviceInfo.getImei(context));
        this.isChanged = isChanged;
    }

    public Authorization(String phoneNumber, Context context) {
        this.phoneNumberHash = WahdaEncryptor.encryptPhoneNumber(phoneNumber);
        this.os = "Android";
        this.device = Build.MODEL;
        this.version = String.valueOf(Build.VERSION.SDK_INT);
        this.ipAddress = DeviceInfo.getIpAddress();
        this.location = DeviceInfo.getLocation(context);
        this.imeiHash = WahdaEncryptor.encryptImeiCode(DeviceInfo.getImei(context));
        this.userName = phoneNumber;//DeviceInfo.getUserName(context);
        this.isChanged = false;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumberHash() {
        return phoneNumberHash;
    }

    public void setPhoneNumberHash(String phoneNumberHash) {
        this.phoneNumberHash = phoneNumberHash;
    }

    public String getImeiHash() {
        return imeiHash;
    }

    public void setImeiHash(String imeiHash) {
        this.imeiHash = imeiHash;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(boolean changed) {
        isChanged = changed;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

