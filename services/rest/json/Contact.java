package mmiit.wahda.services.rest.json;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;

/**
 * Created by MMIIT on 9/21/2018.
 */
public class Contact implements Serializable {
    private Long id;
    private String username;
    private String avatarUrl;
    private String phoneNumber;
    private Boolean hidden;
    private Boolean isActivated;
    private Long lastLogin;
    private Long lastUpdate;
    private boolean isOnline;
    private boolean isBan;
    private String jid;

    public Contact() {
    }

    public Contact(Long id, String username, String jid, String avatarUrl, String phoneNumber, Boolean hidden, Boolean isActivated, Long lastLogin, boolean isOnline) {
        this.id = id;
        this.username = username;
        this.avatarUrl = avatarUrl;
        this.phoneNumber = phoneNumber;
        this.hidden = hidden;
        this.isActivated = isActivated;
        this.lastLogin = lastLogin;
        this.isOnline = isOnline;
        this.jid = jid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean activated) {
        isActivated = activated;
    }

    public Long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public boolean isBan() {
        return isBan;
    }

    public void setBan(boolean ban) {
        isBan = ban;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getJid() {
        return jid;
    }

    public String toFullJid(){
        if(jid.contains("@wahda")){
            return jid;
        }
        return new StringBuilder().append(jid).append("@wahda").toString();
    }
    public void setJid(String jid) {
        this.jid = jid;
    }
}
